'use strict'

// node js
var https       = require('https');
var querystring = require('querystring');

// dependencies
var express     = require("express");
var request     = require('request');
var app         = express();

// vars
var confirm = "ba30c756";



// VK requests
// http://serial-bot.herokuapp.com/vkapi

app.post('/vkapi', function (req, res) {

  switch (req.body.type) {

    // Confirm app

    case "confirmation":
      res.send(confirm);
      break;

});



// Listen

var port = process.env.PORT || 3000;

app.listen(port, function() {
  console.log("Listening on " + port);
});
